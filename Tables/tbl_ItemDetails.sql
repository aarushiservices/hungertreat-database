USE [foodcourt]
GO

/****** Object:  Table [dbo].[tbl_ItemDetails]    Script Date: 2015-11-10 18:48:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tbl_ItemDetails](
	[ItemDetailsID] [int] IDENTITY(1,1) NOT NULL,
	[ItemID] [int] NOT NULL,
	[ItemCode] [varchar](50) NOT NULL,
	[ItemSize] [varchar](50) NOT NULL,
	[SalesPrice] [varchar](50) NOT NULL,
	[TaxPrice] [varchar](50) NOT NULL,
	[Discount] [varchar](50) NOT NULL,
	[UOM] [varchar](50) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[LastEditBy] [int] NOT NULL,
	[LastEditOn] [datetime] NOT NULL,
 CONSTRAINT [PK_tbl_ItemDetails] PRIMARY KEY CLUSTERED 
(
	[ItemDetailsID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[tbl_ItemDetails]  WITH NOCHECK ADD  CONSTRAINT [FK_tbl_ItemDetails_tbl_Item] FOREIGN KEY([ItemID])
REFERENCES [dbo].[tbl_Item] ([ItemID])
GO

ALTER TABLE [dbo].[tbl_ItemDetails] CHECK CONSTRAINT [FK_tbl_ItemDetails_tbl_Item]
GO


