USE [foodcourt]
GO

/****** Object:  Table [dbo].[tbl_Item]    Script Date: 2015-11-10 18:48:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tbl_Item](
	[ItemID] [int] IDENTITY(1,1) NOT NULL,
	[CategoryID] [int] NOT NULL,
	[ItemName] [varchar](50) NOT NULL,
	[ItemDescription] [varchar](4000) NOT NULL,
	[ItemImage] [varchar](50) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[LastEditBy] [int] NOT NULL,
	[LastEditOn] [datetime] NOT NULL,
 CONSTRAINT [PK_tbl_Item] PRIMARY KEY CLUSTERED 
(
	[ItemID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[tbl_Item]  WITH NOCHECK ADD  CONSTRAINT [FK_tbl_Item_tbl_Category] FOREIGN KEY([CategoryID])
REFERENCES [dbo].[tbl_Category] ([CategoryID])
GO

ALTER TABLE [dbo].[tbl_Item] CHECK CONSTRAINT [FK_tbl_Item_tbl_Category]
GO


