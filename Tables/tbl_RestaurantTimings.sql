USE [foodcourt]
GO

/****** Object:  Table [dbo].[tbl_RestaurantTimings]    Script Date: 2015-11-10 18:50:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tbl_RestaurantTimings](
	[RestaurantTimingsID] [int] IDENTITY(1,1) NOT NULL,
	[RestaurantID] [int] NOT NULL,
	[Day] [varchar](50) NOT NULL,
	[StartTime] [varchar](50) NOT NULL,
	[EndTime] [varchar](50) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[LastEditBy] [int] NOT NULL,
	[LastEditOn] [datetime] NOT NULL,
 CONSTRAINT [PK_tbl_RestaurantTimings] PRIMARY KEY CLUSTERED 
(
	[RestaurantTimingsID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[tbl_RestaurantTimings]  WITH NOCHECK ADD  CONSTRAINT [FK_tbl_Restaurant_tbl_RestaurantTimings] FOREIGN KEY([RestaurantID])
REFERENCES [dbo].[tbl_Restaurant] ([RestaurantID])
GO

ALTER TABLE [dbo].[tbl_RestaurantTimings] CHECK CONSTRAINT [FK_tbl_Restaurant_tbl_RestaurantTimings]
GO


