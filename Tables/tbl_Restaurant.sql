USE [foodcourt]
GO

/****** Object:  Table [dbo].[tbl_Restaurant]    Script Date: 2015-11-10 18:49:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tbl_Restaurant](
	[RestaurantID] [int] IDENTITY(1,1) NOT NULL,
	[RestaurantName] [varchar](200) NOT NULL,
	[RestaurantShortName] [varchar](10) NOT NULL,
	[RestaurantDescription] [varchar](500) NOT NULL,
	[TAXID] [varchar](200) NOT NULL,
	[PrimaryContactName] [varchar](50) NULL,
	[PrimaryContactEmail] [varchar](50) NULL,
	[PrimaryContactNumber] [varchar](50) NULL,
	[IsActive] [bit] NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[LastEditBy] [int] NOT NULL,
	[LastEditOn] [datetime] NOT NULL,
 CONSTRAINT [PK_tbl_Restaurant] PRIMARY KEY CLUSTERED 
(
	[RestaurantID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


