USE [foodcourt]
GO

/****** Object:  Table [dbo].[tbl_Section]    Script Date: 2015-11-10 18:50:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tbl_Section](
	[SectionID] [int] IDENTITY(1,1) NOT NULL,
	[RestaurantID] [int] NOT NULL,
	[SectionName] [varchar](50) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[LastEditBy] [int] NOT NULL,
	[LastEditOn] [datetime] NOT NULL,
 CONSTRAINT [PK_tbl_Section] PRIMARY KEY CLUSTERED 
(
	[SectionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[tbl_Section]  WITH NOCHECK ADD  CONSTRAINT [FK_tbl_Restaurant_tbl_Section] FOREIGN KEY([RestaurantID])
REFERENCES [dbo].[tbl_Restaurant] ([RestaurantID])
GO

ALTER TABLE [dbo].[tbl_Section] CHECK CONSTRAINT [FK_tbl_Restaurant_tbl_Section]
GO


