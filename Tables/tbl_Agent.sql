USE [foodcourt]
GO

/****** Object:  Table [dbo].[tbl_Agent]    Script Date: 2015-11-10 18:47:46 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tbl_Agent](
	[AgentID] [int] IDENTITY(1,1) NOT NULL,
	[AgentName] [varchar](50) NOT NULL,
	[AgentPhone] [varchar](50) NOT NULL,
	[AgentEmailID] [varchar](200) NOT NULL,
	[AgentPercentage] [char](5) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[LastEditBy] [int] NOT NULL,
	[LastEditOn] [datetime] NOT NULL,
 CONSTRAINT [PK_tbl_Agent] PRIMARY KEY CLUSTERED 
(
	[AgentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[tbl_Agent] ADD  CONSTRAINT [DF_tbl_Agent_CreatedOn]  DEFAULT (getdate()) FOR [CreatedOn]
GO

ALTER TABLE [dbo].[tbl_Agent] ADD  CONSTRAINT [DF_tbl_Agent_LastEditOn]  DEFAULT (getdate()) FOR [LastEditOn]
GO


