USE [foodcourt]
GO

/****** Object:  Table [dbo].[tbl_RestaurantImage]    Script Date: 2015-11-10 18:49:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tbl_RestaurantImage](
	[RestaurantImageID] [int] IDENTITY(1,1) NOT NULL,
	[RestaurantID] [int] NOT NULL,
	[RestaurantImage] [varchar](500) NOT NULL,
 CONSTRAINT [PK_tbl_RestaurantImage] PRIMARY KEY CLUSTERED 
(
	[RestaurantImageID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


