USE [foodcourt]
GO

/****** Object:  Table [dbo].[tbl_Order]    Script Date: 2015-11-10 18:49:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tbl_Order](
	[OrderID] [int] IDENTITY(1,1) NOT NULL,
	[CustomerID] [int] NOT NULL,
	[RestaurantID] [int] NOT NULL,
	[lk_OrderStatus] [int] NOT NULL,
	[lk_OrderType] [int] NOT NULL,
	[OrderAmount] [varchar](200) NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[LastEditBy] [int] NOT NULL,
	[LastEditOn] [datetime] NOT NULL,
 CONSTRAINT [PK_tbl_Order] PRIMARY KEY CLUSTERED 
(
	[OrderID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[tbl_Order]  WITH NOCHECK ADD  CONSTRAINT [FK_tbl_Order_tbl_Customer] FOREIGN KEY([CustomerID])
REFERENCES [dbo].[tbl_Customer] ([CustomerID])
GO

ALTER TABLE [dbo].[tbl_Order] CHECK CONSTRAINT [FK_tbl_Order_tbl_Customer]
GO

ALTER TABLE [dbo].[tbl_Order]  WITH NOCHECK ADD  CONSTRAINT [FK_tbl_Order_tbl_Restaurant] FOREIGN KEY([RestaurantID])
REFERENCES [dbo].[tbl_Restaurant] ([RestaurantID])
GO

ALTER TABLE [dbo].[tbl_Order] CHECK CONSTRAINT [FK_tbl_Order_tbl_Restaurant]
GO


