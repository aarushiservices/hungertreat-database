USE [foodcourt]
GO

/****** Object:  Table [dbo].[tbl_RestaurantDetails]    Script Date: 2015-11-10 18:49:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tbl_RestaurantDetails](
	[RestaurantDetailsID] [int] IDENTITY(1,1) NOT NULL,
	[RestaurantID] [int] NOT NULL,
	[Address1] [varchar](200) NOT NULL,
	[Address2] [varchar](200) NOT NULL,
	[City] [varchar](50) NOT NULL,
	[State] [varchar](50) NOT NULL,
	[Country] [varchar](50) NOT NULL,
	[ZIPCode] [varchar](50) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[LastEditBy] [int] NOT NULL,
	[LastEditOn] [datetime] NOT NULL,
 CONSTRAINT [PK_tbl_RestaurantDetails] PRIMARY KEY CLUSTERED 
(
	[RestaurantDetailsID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[tbl_RestaurantDetails]  WITH NOCHECK ADD  CONSTRAINT [FK_tbl_Restaurant_tbl_RestaurantDetails] FOREIGN KEY([RestaurantID])
REFERENCES [dbo].[tbl_Restaurant] ([RestaurantID])
GO

ALTER TABLE [dbo].[tbl_RestaurantDetails] CHECK CONSTRAINT [FK_tbl_Restaurant_tbl_RestaurantDetails]
GO


