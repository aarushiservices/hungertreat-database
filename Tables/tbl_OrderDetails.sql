USE [foodcourt]
GO

/****** Object:  Table [dbo].[tbl_OrderDetails]    Script Date: 2015-11-10 18:49:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tbl_OrderDetails](
	[OrderDetailsID] [int] IDENTITY(1,1) NOT NULL,
	[OrderID] [int] NOT NULL,
	[ItemDetailsID] [int] NOT NULL,
	[ItemQuantity] [int] NOT NULL,
	[ItemOrderNotes] [varchar](200) NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[LastEditBy] [int] NOT NULL,
	[LastEditOn] [datetime] NOT NULL,
 CONSTRAINT [PK_tbl_OrderDetails] PRIMARY KEY CLUSTERED 
(
	[OrderDetailsID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[tbl_OrderDetails]  WITH NOCHECK ADD  CONSTRAINT [FK_tbl_OrderDetails_tbl_ItemDetails] FOREIGN KEY([ItemDetailsID])
REFERENCES [dbo].[tbl_ItemDetails] ([ItemDetailsID])
GO

ALTER TABLE [dbo].[tbl_OrderDetails] CHECK CONSTRAINT [FK_tbl_OrderDetails_tbl_ItemDetails]
GO

ALTER TABLE [dbo].[tbl_OrderDetails]  WITH NOCHECK ADD  CONSTRAINT [FK_tbl_OrderDetails_tbl_Order] FOREIGN KEY([OrderID])
REFERENCES [dbo].[tbl_Order] ([OrderID])
GO

ALTER TABLE [dbo].[tbl_OrderDetails] CHECK CONSTRAINT [FK_tbl_OrderDetails_tbl_Order]
GO


