USE [foodcourt]
GO

/****** Object:  Table [dbo].[tbl_Lookup]    Script Date: 2015-11-10 18:48:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tbl_Lookup](
	[LookupID] [int] IDENTITY(1,1) NOT NULL,
	[LookupCategoryID] [int] NOT NULL,
	[LookupName] [varchar](50) NOT NULL,
	[LookupCode] [char](2) NOT NULL,
	[LookupDescription] [varchar](200) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[LastEditBy] [int] NOT NULL,
	[LastEditOn] [datetime] NOT NULL,
 CONSTRAINT [PK_tbl_Lookup] PRIMARY KEY CLUSTERED 
(
	[LookupID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[tbl_Lookup] ADD  CONSTRAINT [DF_tbl_Lookup_CreatedOn]  DEFAULT (getdate()) FOR [CreatedOn]
GO

ALTER TABLE [dbo].[tbl_Lookup] ADD  CONSTRAINT [DF_tbl_Lookup_LastEditOn]  DEFAULT (getdate()) FOR [LastEditOn]
GO

ALTER TABLE [dbo].[tbl_Lookup]  WITH CHECK ADD  CONSTRAINT [FK_tbl_Lookup_tbl_LookupCategory] FOREIGN KEY([LookupCategoryID])
REFERENCES [dbo].[tbl_LookupCategory] ([LookupCategoryID])
GO

ALTER TABLE [dbo].[tbl_Lookup] CHECK CONSTRAINT [FK_tbl_Lookup_tbl_LookupCategory]
GO


