USE [foodcourt]
GO

/****** Object:  Table [dbo].[tbl_Category]    Script Date: 2015-11-10 18:47:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tbl_Category](
	[CategoryID] [int] IDENTITY(1,1) NOT NULL,
	[SectionID] [int] NOT NULL,
	[CategoryName] [varchar](50) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[LastEditBy] [int] NOT NULL,
	[LastEditOn] [datetime] NOT NULL,
 CONSTRAINT [PK_tbl_Category] PRIMARY KEY CLUSTERED 
(
	[CategoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[tbl_Category]  WITH NOCHECK ADD  CONSTRAINT [FK_tbl_Section_tbl_Category] FOREIGN KEY([SectionID])
REFERENCES [dbo].[tbl_Section] ([SectionID])
GO

ALTER TABLE [dbo].[tbl_Category] CHECK CONSTRAINT [FK_tbl_Section_tbl_Category]
GO


