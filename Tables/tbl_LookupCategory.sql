USE [foodcourt]
GO

/****** Object:  Table [dbo].[tbl_LookupCategory]    Script Date: 2015-11-10 18:49:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tbl_LookupCategory](
	[LookupCategoryID] [int] IDENTITY(1,1) NOT NULL,
	[LookupCategoryName] [varchar](50) NOT NULL,
	[LookupCategoryCode] [char](2) NOT NULL,
	[LookupCategoryDescription] [varchar](200) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[LastEditBy] [int] NOT NULL,
	[LastEditOn] [datetime] NOT NULL,
 CONSTRAINT [PK_tbl_LookupCategory] PRIMARY KEY CLUSTERED 
(
	[LookupCategoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[tbl_LookupCategory] ADD  CONSTRAINT [DF_tbl_LookupCategory_CreatedOn]  DEFAULT (getdate()) FOR [CreatedOn]
GO

ALTER TABLE [dbo].[tbl_LookupCategory] ADD  CONSTRAINT [DF_tbl_LookupCategory_LastEditOn]  DEFAULT (getdate()) FOR [LastEditOn]
GO


